/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nymr3kt.swing;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 * @author nymr3kt
 */
class myActionListener implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e){
        System.out.println("MyActionLIstener : Action");
    }
}


public class FrameTest implements ActionListener{

    public static void main(String[] args) {
        JFrame f = new JFrame("Frame Test");
        f.setSize(450, 200);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel l1 = new JLabel("Your Name:");
        l1.setSize(80, 20);
        l1.setLocation(5, 5);
        l1.setOpaque(true);

        JTextField t1 = new JTextField();
        t1.setSize(200, 20);
        t1.setLocation(90, 5);
        
        JButton b1 = new JButton("Enter");
        b1.setSize(80,20);
        b1.setLocation(90,40);
        
        myActionListener ac1 = new myActionListener() ;                
        b1.addActionListener(ac1);
        b1.addActionListener(new FrameTest());
        
        ActionListener action = new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Anonymous Class : Action");
            }
            
        };
        b1.addActionListener(action);
        
        JLabel l2 = new JLabel("Hello--> ",JLabel.CENTER);
        l2.setSize(200,20);
        l2.setLocation(90, 70);
        l2.setOpaque(true);

        f.setLayout(null);

        f.add(l1);
        f.add(t1);
        f.add(b1);
        f.add(l2);

        b1.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = t1.getText();
                l2.setText("Hello--> "+name);
            }
            
        });
        
         f.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
          System.out.println("FrameTest Action");
    }
}
